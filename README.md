# README #

PhotoBot is a simple Twitter application which uses the Requests library to parse PhillyHistory.org's rich gallery of photographs of Philadelphia dating from the very beginning of photography itself in the early 19th Century through the 20th Century, courtesy of the City of Philadelphia Department of Records. Photos are tweeted with original title and date, if available.

http://www.phillyhistory.org/PhotoArchive/Home.aspx

The bot can be found at:
https://twitter.com/OldPhillyPhotos