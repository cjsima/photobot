argparse==1.2.1
lxml==3.4.1
requests==2.4.3
tweepy==2.3.0
urllib3==1.9.1
wsgiref==0.1.2
