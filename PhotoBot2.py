import tweepy
import urllib
import requests
import sys
import os

BASE_URL = 'http://www.phillyhistory.org/PhotoArchive/'
QUERY_URL = BASE_URL + 'Thumbnails.ashx'
DETAILS_URL = BASE_URL + 'Details.ashx'

def get_media_url(asset_id):
	response = requests.post(DETAILS_URL, data={'assetId': asset_id})
	image_details = response.json()
	media_id = image_details['assets'][0]['medialist'][0]['mediaId']
	return '{}/MediaStream.ashx?mediaId={}'.format(BASE_URL, media_id)

def save_image(asset_id):
	filename = '{}.jpg'.format(asset_id)
	url = get_media_url(asset_id)

	with open(filename, 'wb') as f:
		response = requests.get(url)
		f.write(response.content)
	return filename

def mark_tweeted(asset_id):
	with open('Projects/Twitter/photobot/tweeted_photos.txt', 'at') as f:
		f.write(str(asset_id)+ " ")

def tweet(status, date, filename):
	CONSUMER_KEY = 'glsskeym4tEafODriw5DEr3wt'
	CONSUMER_SECRET = 'Jh2UbxKOWA3kcCxxr0UX1jgPEgIkgJQr6QFLWE3ow9nfozEQsN'
	ACCESS_KEY = '2895059483-IjnigQ1mKHVxSou8hlvAyHPdW5ZwifG4d0kBDzY'
	ACCESS_SECRET = '730VLXSvxLZVwOXQ7QNTaZaCZVTHfrBZakQqFV2UmN9Tx'
	auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
	auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)
	# access the Twitter API using tweepy with OAuth
	api = tweepy.API(auth)
	if date:
		status = status + ", " + str(date)
	else:
		status = status
	filepath = os.path.abspath(filename)
	#UpdateStatus of twitter called with the image file
	api.update_with_media(filepath, status=status)
	"""Follow back those that follow this account"""
	for follower in tweepy.Cursor(api.followers).items():
		try:
			follower.follow()
		except:
			pass

urlqs = {
    'maxx': '-8321310.550067',
    'maxy': '4912533.794965',
    'minx': '-8413034.983992',
    'miny': '4805521.955385',
    'onlyWithoutLoc': 'false',
    'sortOrderM': 'DISTANCE',
    'sortOrderP': 'DISTANCE',
    'type': 'area',
    'updateDays': '0',
    'withoutLoc': 'false',
    'withoutMedia': 'false'
}

data = {
    'start': 0,
    'limit': 10000,
    'noStore': 'false',
    'request': 'Images',
    'urlqs': urllib.urlencode(urlqs)
}

response = requests.post(QUERY_URL, data=data)
result = response.json()

# print '{} images found'.format(result['totalImages'])

for image in result['images']:
	asset_id = image['assetId']
	name = image['name']
	date = image['date']
	filename = save_image(asset_id)

	print 'Name: {}'.format(name)
	print 'Asset ID: {}'.format(asset_id)

	with open('Projects/Twitter/photobot/tweeted_photos.txt', 'rt') as f:
		tweets = f.read()
	if str(asset_id) not in tweets:
		save_image(asset_id)
		print "IMAGE SAVED"
		mark_tweeted(asset_id)
		tweet(name, date, filename)
		print "TWEETED"
		sys.exit()

